import itertools
import random
import warnings
import time
import json
import os.path
import datetime

from collections import namedtuple
from collections import Counter

from isolation import Board
from sample_players import (RandomPlayer, open_move_score,
                            improved_score, center_score)
from game_agent import (MinimaxPlayer, AlphaBetaPlayer, custom_score,
                        custom_score_2, custom_score_3)


Agent = namedtuple("Agent", ["player", "name"])
NUM_MATCHES = 5  # number of matches against each opponent
TIME_LIMIT = 150  # number of milliseconds before timeout
DATASET_PATH = "datasets/dataset.json"
DATASET_INFO_PATH = "datasets/dataset_info.json"


def play_batch(player1, player2, num_matches, batch_index):
    """Compare the test agents to the cpu agent in "fair" matches.

    "Fair" matches use random starting locations and force the agents to
    play as both first and second player to control for advantages resulting
    from choosing better opening moves or having first initiative to move.
    """
    timeout_count = 0
    forfeit_count = 0
    # games won by player1
    games_won = 0
    win_move_counts = {}
    for match_id in range(num_matches):
        if match_id % 10 == 0:
            print("Playing match {0} of batch {1}".format(match_id, batch_index))

        if match_id % 2 == 0:
            game = Board(player1, player2)
        else:
            game = Board(player2, player1)

        # initialize all games with a random move and response
        for _ in range(2):
            move = random.choice(game.get_legal_moves())
            game.apply_move(move)
        # play a game
        winner, move_history, termination = game.play(time_limit=TIME_LIMIT)

        win_moves = []
        # we start from second moves, becase first move of each player is random
        if match_id % 2 == 0:
            # in even match indices player 1 moves first, his second move index is 2
            if winner == player1:
                # first move is random, we don't take it into account
                win_moves = move_history[2::2]
        else:
            # in odd matches player 2 moves first, so player1 has second move at index 3
            if winner == player1:
                win_moves = move_history[3::2]

        # record if player1 has won
        if winner == player1:
            games_won += 1

        for move in win_moves:
            move_key = str(move[0]) + ',' + str(move[1])
            if move_key not in win_move_counts:
                win_move_counts[move_key] = 1
            else:
                win_move_counts[move_key] += 1

        if termination == "timeout":
            timeout_count += 1
        elif winner not in opponents and termination == "forfeit":
            forfeit_count += 1

    return timeout_count, forfeit_count, win_move_counts, games_won


def simulate(player1, opponents, batch_size):
    dataset = read_from_file(DATASET_PATH) if os.path.isfile(DATASET_PATH) else {}
    dataset_info = read_from_file(DATASET_INFO_PATH) if os.path.isfile(DATASET_INFO_PATH) else init_dataset_info()
    batch_index = 0
    start_time = time.time()
    for opponent in opponents:
        print("Starting batch {0}...".format(batch_index))
        
        timeout_count, forfeit_count, win_move_counts, games_won = play_batch(player1, opponent.player, batch_size, batch_index)
        time_elapsed = time.time() - start_time
        win_rate = games_won / float(batch_size)
        dataset = append_dataset(dataset, win_move_counts)
        dataset_info['time_elapsed'] += time_elapsed
        dataset_info['games_played'] += batch_size
        dataset_info['games_won'] += games_won

        print("-------------------------------------------------------")
        print("Batch {0} finished:".format(batch_index))
        print("Time elapsed: {0}".format(time_elapsed))
        print("Opponent: {0}".format(opponent.name))
        print("Games played: {0}".format(batch_size))
        print("Games won: {0}".format(games_won))
        print("Win rate: {0}".format(win_rate))
        print("Games timed out: {0}".format(timeout_count))
        print("Games forfeited: {0}".format(forfeit_count))
        print("-------------------------------------------------------")

        batch_index += 1

    write_to_file(dataset, DATASET_PATH)
    write_to_file(dataset_info, DATASET_INFO_PATH)


def write_to_file(dict, path):
    json.dump(dict, open(path, 'w'))


def read_from_file(path):
    return json.load(open(path))


def append_dataset(dataset, game_moves):
    a = Counter(dataset)
    b = Counter(game_moves)
    return a + b


def init_dataset_info():
    info = {}
    info['time_elapsed'] = 0
    info['games_played'] = 0
    info['games_won'] = 0

    return info


if __name__ == "__main__":
    my_player = AlphaBetaPlayer(score_fn=custom_score)
    opponents = [
        # Agent(RandomPlayer(), "Random"),
        Agent(MinimaxPlayer(score_fn=open_move_score), "MM_Open"),
        Agent(MinimaxPlayer(score_fn=center_score), "MM_Center"),
        Agent(MinimaxPlayer(score_fn=improved_score), "MM_Improved"),
        Agent(AlphaBetaPlayer(score_fn=open_move_score), "AB_Open"),
        Agent(AlphaBetaPlayer(score_fn=center_score), "AB_Center"),
        Agent(AlphaBetaPlayer(score_fn=improved_score), "AB_Improved")]
    
    simulate(my_player, opponents, 1000)
